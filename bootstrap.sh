#!/usr/bin/env bash

apt-get -q update
apt-get -qqy install python-pip python-dev build-essential
pip install --upgrade pip
pip install google-api-python-client
pip install Flask
pip install WTForms
pip install SQLAlchemy
pip install oauth2client
pip install --upgrade oauth2client
