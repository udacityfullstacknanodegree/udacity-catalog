from wtforms import Form, StringField, TextAreaField, HiddenField
from wtforms import SubmitField, ValidationError
from wtforms.validators import Length, Required
from config.database_setup import session, Categories


class CategoryForm(Form):
    name = StringField('Name', [Length(min=3, max=250), Required()])
    description = TextAreaField('Description', [Length(max=250)])
    category_id = HiddenField()
    submit = SubmitField()

    def validate_name(form, field):
        category = session.query(Categories).filter_by(name=field.data.title())
        if form.category_id.data:
            category = category.filter(Categories.id != form.category_id.data)
        category = category.first()
        if category:
            raise ValidationError('A category %s already exists!' % field.data)


class DeleteCategoryForm(Form):
    category_id = HiddenField(Required())
    submit = SubmitField("Yes, delete it!")
