from wtforms import Form, ValidationError, SubmitField
from wtforms import StringField, HiddenField, SelectField, TextAreaField
from wtforms.validators import Length, Required
from config.database_setup import session, Categories, Items


class ItemForm(Form):
    name = StringField('Name', [Length(min=3, max=250), Required()])
    description = TextAreaField('Description', [Length(min=3, max=250),
                                                Required()])
    category_id = SelectField("Category", [Required()], coerce=int)
    item_id = HiddenField()
    submit = SubmitField()

    def validate_name(form, field):
        """ Check if item's name already exists in the category_id

            :returns:
                `ValidationError` if the name already exists
        """
        item = session.query(Items)
        item = item.filter_by(name=field.data.title(),
                              category_id=form.category_id.data)
        # The current item is exclude from the search
        if form.item_id.data:
            item = item.filter(Items.id != form.item_id.data)

        item = item.first()
        if item:
            raise ValidationError("An item %s already exists " % field.data
                                  + "in this category!")


class DeleteItemForm(Form):
    category_id = HiddenField(Required())
    item_id = HiddenField(Required())
    submit = SubmitField("Yes, delete it!")
