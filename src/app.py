from flask import Flask, render_template, request, redirect, url_for, jsonify
from flask import flash, session as login_session, make_response

from config.database_setup import session, Users, Categories, Items

from forms.category import CategoryForm, DeleteCategoryForm
from forms.item import ItemForm, DeleteItemForm

from oauth2client import client, crypt
import random
import string
import json

from functools import wraps

app = Flask(__name__)
ITEMS_PER_PAGE = 6


def login_required(f):
    """ Check if user is logged in
    """
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if "user_id" not in login_session:
            return redirect(url_for('showLogin'))

        return f(*args, **kwargs)
    return decorated_function


@app.route("/login")
def showLogin():
    """ Login page
    """
    categories = getAllCategories()

    # State string used during login process
    state = ''.join(random.choice(string.ascii_uppercase + string.digits)
                    for x in xrange(32))
    login_session['state'] = state

    return render_template("login.html",
                           categories=categories,
                           STATE=state)


@app.route("/logout")
@login_required
def GLogout():
    """ Logout the user

    This route is called only if G+ SignOut() call has been successful
    """
    for key in login_session.keys():
        del login_session[key]
    return json_response("Successfully disconnected.", 200)


@app.route("/gconnect", methods=["POST"])
def GConnect():
    """ Login the user

    This route is called only after if G+ SignIn() has been successful
    """
    state = request.form.get('state')
    token = request.form.get('token')

    # Check State
    if state != login_session['state']:
        return json_response("Invalid state parameter", 401)

    # Check Token validity and retrieve user info
    if not token:
        return json_response("Missing token", 401)
    try:
        d = loadJSONFile("client_secret")  # Retrieve app ID via jsonfile
        idinfo = client.verify_id_token(token, d['web']['client_id'])

        if idinfo['iss'] not in ['accounts.google.com',
                                 'https://accounts.google.com']:
            raise crypt.AppIdentityError("Wrong issuer")

    except crypt.AppIdentityError as e:
        return json_response(str(e), 401)

    # Redirect user if already logged in
    user_id = idinfo['sub']

    stored_idinfo = login_session.get('idinfo')
    stored_user_id = login_session.get('user_id')
    if stored_idinfo is not None and user_id == stored_user_id:
        return json_response("User is already logged in", 200)

    # Save user info in session
    login_session['idinfo'] = idinfo
    login_session['user_id'] = user_id
    login_session['name'] = idinfo['name']
    login_session['picture'] = idinfo['picture']
    login_session['email'] = idinfo['email']

    # Register the user in database
    if getUserInfo(user_id):
        flash("You are now logged in as %s" % login_session['name'],
              "success")
        return json_response("Connection successful", 200)
    elif createUser(login_session):
        flash("Welcome %s! You are now logged in" % login_session['name'],
              "success")
        return json_response("Creation and connection successful", 200)
    else:
        return json_response("Impossible to retrieve/create the user", 401)


@app.route("/catalog.json")
def showCatalogJSON():
    """ Generate JSON file of the catalog

    Optional parameters can be used to filter the request
    /catalog.json?category_id={int}&offset={int}

    :param category_id:
        ID of the category we want to filter
        If not specified, all categories are retrieved

    :param offset:
        Used for pagination purpose,
        retrieve items from the given offset

    :return:
        Catalog in JSON format
    """
    category_id = request.args.get('category_id')
    offset = request.args.get('offset')

    if category_id and category_id.isdigit():
        category_id = int(category_id)
    else:
        category_id = None

    if offset and offset.isdigit():
        items = getItems(category_id=category_id, offset=int(offset))
    else:
        items = getAllItems(category_id=category_id)

    catalogItems = [i.serialize for i in items]
    return jsonify(catalogItems)


@app.route("/")
@app.route("/catalog")
def showCatalog():
    """ Show catalog

    List all items from all categories
    """
    categories = getAllCategories()
    total_items = countItems()

    return render_template("catalog/view.html",
                           categories=categories,
                           items=total_items)


@app.route("/catalog/new", methods=["GET", "POST"])
@login_required
def newCategory():
    """ Create new category
    """
    categories = getAllCategories()
    form = CategoryForm(request.form)

    if request.method == "POST" and form.validate():
        category = Categories(name=form.name.data.title(),
                              description=form.description.data,
                              user_id=login_session["user_id"])
        try:
            session.add(category)
            session.commit()

            flash("Category \"%s\" has been " % category.name
                  + "successfully created",
                  "success")
            return redirect(url_for("showCategory",
                                    category_id=category.id))
        except Exception as e:
            print str(e)
            session.rollback()

    return render_template("catalog/new.html",
                           categories=categories,
                           form=form)


@app.route("/catalog/<int:category_id>")
@app.route("/catalog/<int:category_id>/items")
def showCategory(category_id):
    """ Show category

    List all items from a given category
    """
    categories = getAllCategories()
    category = getCategory(category_id)
    total_items = countItems(category_id=category_id)

    return render_template("catalog/view.html",
                           categories=categories,
                           category=category,
                           items=total_items)


@app.route("/catalog/<int:category_id>/edit", methods=["GET", "POST"])
@login_required
def editCategory(category_id):
    """ Edit category

    User can only edit its own categories
    """
    categories = getAllCategories()
    category = getCategory(category_id)
    form = CategoryForm(request.form, obj=category, category_id=category_id)

    # Redirect user if not authorized to edit
    if category.user.id != login_session["user_id"]:
        flash("You are not authorized to edit this category",
              "error")
        return redirect(url_for("showCategory", category_id=category_id))

    if (request.method == "POST" and form.validate() and
            int(form.category_id.data) == category_id):

        category.name = form.name.data.title()
        category.description = form.description.data
        try:
            session.add(category)
            session.commit()

            flash("Category \"%s\" has been " % category.name
                  + "successfully edited",
                  "success")
            return redirect(url_for("showCategory",
                                    category_id=category.id))
        except Exception as e:
            print str(e)
            session.rollback()

    return render_template("catalog/edit.html",
                           categories=categories,
                           category=category,
                           form=form)


@app.route("/catalog/<int:category_id>/delete", methods=["GET", "POST"])
@login_required
def deleteCategory(category_id):
    """ Delete category

    User can only delete its own categories
    All items in the category will be removed
    """
    categories = getAllCategories()
    category = getCategory(category_id)
    form = DeleteCategoryForm(request.form, category_id=category_id)

    # Redirect user if not authorized to delete
    if category.user.id != login_session["user_id"]:
        flash("You are not authorized to delete this category",
              "error")
        return redirect(url_for("showCategory", category_id=category_id))

    if (request.method == "POST" and form.validate() and
            int(form.category_id.data) == category_id):
        try:
            session.delete(category)
            session.commit()

            flash("Category \"%s\" has been " % category.name
                  + "successfully deleted",
                  "success")
            return redirect(url_for("showCatalog"))
        except Exception as e:
            print str(e)
            session.rollback()

    return render_template("catalog/delete.html",
                           categories=categories,
                           category=category,
                           form=form)


@app.route("/catalog/items/new", methods=["GET", "POST"],
           defaults={'category_id': ''})
@app.route("/catalog/<int:category_id>/items/new", methods=["GET", "POST"])
@login_required
def newItem(category_id):
    """ Create new item
    """
    categories = getAllCategories()
    category = getCategory(category_id)
    form = ItemForm(request.form, category_id=category_id)
    form.category_id.choices = [(c.id, c.name) for c in categories]

    if request.method == "POST" and form.validate():
        item = Items(name=form.name.data.title(),
                     description=form.description.data,
                     category_id=form.category_id.data,
                     user_id=login_session['user_id'])
        try:
            session.add(item)
            session.commit()

            flash("Item \"%s\" has been " % item.name
                  + "successfully created",
                  "success")
            return redirect(url_for("showItem",
                                    category_id=item.category_id,
                                    item_id=item.id))
        except Exception as e:
            print str(e)
            session.rollback()

    return render_template("item/new.html",
                           categories=categories,
                           category=category,
                           form=form)


@app.route("/catalog/<int:category_id>/items/<int:item_id>")
def showItem(category_id, item_id):
    """ Show item
    """
    categories = getAllCategories()
    category = getCategory(category_id)
    item = getItem(item_id)

    return render_template("item/view.html",
                           categories=categories,
                           category=category,
                           item=item)


@app.route("/catalog/<int:category_id>/items/<int:item_id>/edit",
           methods=["GET", "POST"])
@login_required
def editItem(category_id, item_id):
    """ Edit item

    User can only update its own items
    """
    categories = getAllCategories()
    category = getCategory(category_id)
    item = getItem(item_id)
    form = ItemForm(request.form, obj=item, item_id=item_id)
    form.category_id.choices = [(c.id, c.name) for c in categories]

    # Redirect user if not authorized to edit
    if item.user.id != login_session["user_id"]:
        flash("You are not authorized to edit this item",
              "error")
        return redirect(url_for("showItem",
                                category_id=category_id,
                                item_id=item_id))

    if (request.method == "POST" and form.validate() and
            int(form.item_id.data) == item_id):
        item.name = form.name.data.title()
        item.description = form.description.data
        item.category_id = form.category_id.data
        try:
            session.add(item)
            session.commit()

            flash("Item \"%s\" has been " % item.name
                  + "successfully edited",
                  "success")
            return redirect(url_for("showItem",
                                    category_id=item.category_id,
                                    item_id=item.id))
        except Exception as e:
            print str(e)
            session.rollback()

    return render_template("item/edit.html",
                           categories=categories,
                           category=category,
                           item=item,
                           form=form)


@app.route("/catalog/<int:category_id>/items/<int:item_id>/delete",
           methods=["GET", "POST"])
@login_required
def deleteItem(category_id, item_id):
    """ Delete item

    User can only delete its own items
    """
    categories = getAllCategories()
    category = getCategory(category_id)
    item = getItem(item_id)
    form = DeleteItemForm(request.form, obj=item,
                          category_id=category_id, item_id=item_id)

    # Redirect user if not authorized to delete
    if item.user.id != login_session["user_id"]:
        flash("You are not authorized to delete this item",
              "error")
        return redirect(url_for("showItem",
                                category_id=category_id,
                                item_id=item_id))

    if (request.method == "POST" and form.validate() and
            int(form.category_id.data) == category_id and
            int(form.item_id.data) == item_id):
        try:
            session.delete(item)
            session.commit()

            flash("Item \"%s\" has been " % item.name
                  + "successfully deleted",
                  "success")
            return redirect(url_for("showCategory",
                                    category_id=category_id))
        except Exception as e:
            print str(e)
            session.rollback()

    return render_template("item/delete.html",
                           categories=categories,
                           category=category,
                           item=item,
                           form=form)


# FUNCTIONS
def getAllCategories():
    """ Get all Categories entities

    :returns:
        List of categories entities ordered by name
    """
    try:
        query = session.query(Categories)
        query = query.order_by(Categories.name)
        return query.all()
    except Exception as e:
        print str(e)
        return None


def getCategory(category_id):
    """ Get a specific category entity

        :param category_id:
            Category's ID to retrieve
        :returns:
            Category entity
    """
    try:
        query = session.query(Categories)
        query = query.filter_by(id=category_id)
        query = query.join(Categories.user)
        return query.one()
    except Exception as e:
        print str(e)
        return None


def getAllItems(category_id=None):
    """ Get all items entities

    If a `category_id` is given,
        retrieve all items from this category only

    :param category_id:
        ID category
    :returns:
        List of items entities ordered by creation date
    """
    query = session.query(Items)

    if category_id:
        query = query.filter_by(category_id=category_id)

    query = query.join(Items.category)
    query = query.join(Items.user)

    query = query.order_by(Items.created.desc())
    return query.all()


def getItems(category_id=None, offset=0):
    """ Get {ITEMS_PER_PAGE} items entities

    If a `category_id` is given,
        retrieve items from this category only

    :param category_id:
        ID category
    :param offset:
        Offset to start the selection from
    :returns:
        List of items entities ordered by creation date
    """
    query = session.query(Items)

    if category_id:
        query = query.filter_by(category_id=category_id)

    query = query.join(Items.category)
    query = query.join(Items.user)

    query = query.order_by(Items.created.desc())

    query = query.limit(ITEMS_PER_PAGE)
    query = query.offset(offset)

    return query.all()


def getItem(item_id):
    """ Get item entity

    :param item_id:
        Item's ID
    :returns:
        Item entity
    """
    query = session.query(Items)
    query = query.filter_by(id=item_id)

    return query.one()


def countItems(category_id=None):
    """ Count total items

    If `category_id` is given, count total items in this category only

    :param category_id:
        Category's ID
    :returns:
        Total items
    """
    query = session.query(Items)

    if category_id:
        query = query.filter_by(category_id=category_id)

    query = query.join(Items.category)
    query = query.join(Items.user)

    return query.count()


def getUserInfo(user_id):
    """ Retrieve the user from the database_setup

    :param user_id:
        User's ID
    :returns:
        User entity
    """
    try:
        user = session.query(Users).filter_by(id=user_id).one()
        return user
    except Exception as e:
        print str(e)
        return None


def createUser(login_session):
    """ Create user in database

    :returns:
        If successful, returns the user ID
    """
    user = Users(id=login_session["user_id"],
                 name=login_session["name"],
                 picture=login_session["picture"],
                 email=login_session["email"])
    try:
        session.add(user)
        session.commit()
        return user.id
    except Exception as e:
        print str(e)
        return None


def json_response(message, code):
    """ Create a json response and return it

    :param message:
        Response message
    :param code:
        Response code
    """
    response = make_response(json.dumps(message), code)
    response.headers['Content-Type'] = 'application/json'
    return response


def loadJSONFile(filename):
    """ Read a JSON file and return a dictionnary

    :param filename:
        JSON filename
    :returns:
        Dictionnary
    """
    result = {}

    if (filename.find(".json") == -1):
        filename += ".json"

    with open(filename) as json_data:
        result = json.load(json_data)
    return result


if __name__ == "__main__":
    app.debug = True
    app.secret_key = "S5PEuZ81zGE_C0$Oe@4wskwaYy*a!63h"
    app.run(host="0.0.0.0", port=8000)
