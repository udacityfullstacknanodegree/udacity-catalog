function onLoad() {
  gapi.load('auth2', function() {
    gapi.auth2.init();
  });
}

// G+ Sign out the user
function signOut(redirect) {
  var auth2 = gapi.auth2.getAuthInstance();

  // Redirected by default to /catalog
  if (typeof redirect === 'undefined') {
    redirect = '/catalog';
  }

  /* if signout successful,
      redirect to /logout to disconnect the user on the serverside */
  auth2.signOut().then(function () {
    $.ajax({
      type: 'GET',
      url: '/logout',
      contentType: 'application/x-www-form-urlencoded',
      success: function(result) {
        window.location.href = redirect;
      }
    });
  });
}

// Helpers to create item elements
var HTMLItem = '<div class="item">%data%</div>';
var HTMLItemTitle = '<h3><a href="/catalog/%category_id%/items/%item_id%">%data%</a></h3>';
var HTMLItemDatas = '<p>%data%</p>';
