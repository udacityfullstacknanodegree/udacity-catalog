from sqlalchemy import Column, ForeignKey, Integer, String, DateTime
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, scoped_session
import datetime

Base = declarative_base()


class Users(Base):
    __tablename__ = 'users'

    id = Column(String(250), primary_key=True)
    name = Column(String(250), nullable=False, unique=True)
    email = Column(String(250), nullable=False)
    picture = Column(String(250), nullable=False)
    created = Column(DateTime, default=datetime.datetime.now)
    last_updated = Column(DateTime, onupdate=datetime.datetime.now)

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email,
            'picture': self.picture
        }


class Categories(Base):
    __tablename__ = 'categories'

    user = relationship(Users)
    items = relationship("Items", cascade="delete, delete-orphan")

    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(String(250), ForeignKey('users.id'))
    name = Column(String(250), nullable=False, unique=True)
    description = Column(String(250))
    created = Column(DateTime, default=datetime.datetime.now)
    last_updated = Column(DateTime, onupdate=datetime.datetime.now)

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'id': self.id,
            'name': self.name,
        }


class Items(Base):
    __tablename__ = 'items'

    category = relationship(Categories)
    user = relationship(Users)

    id = Column(Integer, primary_key=True, autoincrement=True)
    category_id = Column(Integer, ForeignKey('categories.id'))
    user_id = Column(String(250), ForeignKey('users.id'))
    name = Column(String(250), nullable=False)
    description = Column(String(250))
    created = Column(DateTime, default=datetime.datetime.now)
    last_updated = Column(DateTime, onupdate=datetime.datetime.now)

    @property
    def serialize(self):
        """Return object data in easily serializeable format"""
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description,
            'user': self.user.serialize,
            'category': self.category.serialize,
            'created': self.created
        }


engine = create_engine('sqlite:///config/catalog.db')
Base.metadata.create_all(engine)

DBSession = sessionmaker(bind=engine)
session = scoped_session(DBSession)
