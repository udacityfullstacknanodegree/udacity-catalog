# CATALOG
Udacity project

## Description
Application that provides a list of items within a variety of categories as well as provide a user registration and authentication system. Registered users have the ability to post, edit and delete their own items.

![Udacity Project](https://i.imgsafe.org/b93eb0c9ce.png)

## Requirements

- VirtualBox
- Vagrant

## Installation

Initialize the VM

`vagrant up`

Connect to the virtual machine

`vagrant ssh`

Navigate to the source directory

`cd /vagrant/src`

Start the server

`python app.py`

Launch the application on your browser by reaching [http://localhost:8000](http://localhost:8000)
